"""
Setup for Proxy Service Scrapy
"""

from setuptools import setup


setup(name='puppeteer-api-scrapy',
      version='1.0',
      description='Scrapy downloader middleware to make requests using the Puppeteer API',
      url='https://gitlab.com/merfrei-solutions/aracness/puppeteer-api-scrapy',
      author='Emiliano M. Rudenick',
      author_email='erude@merfrei.com',
      license='MIT',
      packages=['puppeteer_middleware'],
      install_requires=[
          'w3lib',
      ],
      zip_safe=False)
