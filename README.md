### Puppeteer API - Scrapy Middleware

This is a middleware to use the [**Puppeteer API**](https://gitlab.com/merfrei/puppeteer-api) in your Scrapy's spiders

Spider special attributes. Or `Request.meta` values:

- **pp_enabled**: activate the use of the middleware

- **pp_timeout**: set a custom timeout

- **pp_full**: `fullpage` mode enabled

- **pp_evaluate**: set the `evaluate` parameter with this value

- **pp_headers**: extra headers passed to the **headers** parameter in the URL query

- **pp_click**: passed to the **click** API query parameter.

- **pp_click_wait**: passed to the **click_wait** API query parameter.

- **pp_form**: manage form uploading to the API.

- **pp_firefox**: using Firefox browser instead of Chromium.

- **pp_cookies**: upload cookies, for keeping session.

- **pp_custom**: custom parameters passed to the API. Useful for views accepting more parameters.

- **pp_noagent**: ignore request user agent

- **pp_reajax**: regular expression to match AJAX call URLs and return all of the resulting body contents in a list

- **pp_refilter**:  filter request URLs matching this regular expression. It will include scripts resources anyway. You can use jsoff to exclude them. This option is useful when you want to do only some AJAX calls and exclude others. Useful when using in conjunction with reajax to make it faster.

Data returned in `response.meta`:

- **puppeteer_evaluate_res**: the resultant data of the evaluate function

- **puppeteer_cookies_res**: the page cookies after the response is returned
