"""
Puppeteer API Downloader Middleware
"""

import json

from urllib.parse import quote_plus
from urllib.parse import urljoin

from w3lib.url import add_or_replace_parameter

from twisted.internet.error import TimeoutError as ServerTimeoutError
from twisted.internet.error import TCPTimedOutError
from twisted.internet.error import TCPTimedOutError

from twisted.internet.defer import TimeoutError as UserTimeoutError

from scrapy.http import FormRequest
from scrapy.http import HtmlResponse
from scrapy.http import XmlResponse


class PuppeteerAPIMiddleware(object):

    def __init__(self, settings):
        self.puppeteer_url = settings.get('PUPPETEER_API_URL')
        self.timeout_exceptions = (ServerTimeoutError, UserTimeoutError, TCPTimedOutError)

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings)

    @staticmethod
    def puppeteer_enabled(request, spider):
        if request.url.startswith('file://'):
            return False
        spider_enabled = getattr(spider, 'pp_enabled', False)
        return request.meta.get('pp_enabled', spider_enabled)

    @staticmethod
    def puppeteer_config_value(request, spider, config):
        '''Read the config from meta request first and then from the spider parameters'''
        conf_val = request.meta.get(config)
        if conf_val is None:
            conf_val = getattr(spider, config, None)
        return conf_val

    @classmethod
    def puppeteer_params(cls, request, spider):
        '''It will build the query to the API and return it as a dict object'''
        params = {'url': request.url}
        pp_timeout = cls.puppeteer_config_value(request, spider, 'pp_timeout')
        pp_waituntil = cls.puppeteer_config_value(request, spider, 'pp_waituntil')
        pp_waitforsel = cls.puppeteer_config_value(request, spider, 'pp_waitforsel')
        pp_wait = cls.puppeteer_config_value(request, spider, 'pp_wait')
        pp_full = cls.puppeteer_config_value(request, spider, 'pp_full')
        pp_evaluate = cls.puppeteer_config_value(request, spider, 'pp_evaluate')
        pp_headers = cls.puppeteer_config_value(request, spider, 'pp_headers')
        pp_click = cls.puppeteer_config_value(request, spider, 'pp_click')
        pp_click_wait = cls.puppeteer_config_value(request, spider, 'pp_click_wait')
        pp_form = cls.puppeteer_config_value(request, spider, 'pp_form')
        pp_firefox = cls.puppeteer_config_value(request, spider, 'pp_firefox')
        pp_cookies = cls.puppeteer_config_value(request, spider, 'pp_cookies')
        pp_custom = cls.puppeteer_config_value(request, spider, 'pp_custom')
        pp_reajax = cls.puppeteer_config_value(request, spider, 'pp_reajax')
        pp_refilter = cls.puppeteer_config_value(request, spider, 'pp_refilter')
        pp_rinfo = cls.puppeteer_config_value(request, spider, 'pp_rinfo')
        pp_disres = cls.puppeteer_config_value(request, spider, 'pp_disres')

        user_agent = cls.get_user_agent(spider, request)
        if user_agent is not None:
            params['user_agent'] = user_agent

        proxy = cls.get_proxy_from_request(request, remove_protocol=True)
        if proxy is not None:
            params['proxy'] = proxy

        # It could be 0 too, it will be ignored as well
        if pp_timeout:
            params['timeout'] = str(pp_timeout)

        if pp_full:
            params['fullpage'] = '1'

        if pp_waituntil is not None:
            params['waituntil'] = pp_waituntil

        if pp_waitforsel is not None:
            params['waitforsel'] = pp_waitforsel

        if pp_wait is not None:
            params['wait'] = str(pp_wait)

        if pp_evaluate is not None:
            params['evaluate'] = pp_evaluate

        if pp_headers is not None:
            params['headers'] = pp_headers

        if pp_click is not None:
            params['click'] = pp_click

        if pp_click_wait is not None:
            params['click_wait'] = str(pp_click_wait)

        if pp_form is not None:
            params['form'] = pp_form

        if pp_firefox:
            params['firefox'] = '1'

        if pp_cookies:
            params['cookies'] = pp_cookies

        if pp_custom is not None:
            custom_params = json.loads(pp_custom)
            for pp_pk, pp_pv in custom_params.items():
                params[pp_pk] = pp_pv

        if pp_reajax is not None:
            params['reajax'] = pp_reajax

        if pp_refilter is not None:
            params['refilter'] = pp_refilter

        if pp_rinfo:
            params['rinfo'] = '1'

        if pp_disres:
            params['disres'] = pp_disres

        return params

    @staticmethod
    def get_proxy_from_request(request, remove_protocol=False):
        proxy = request.meta.get('proxy')
        if proxy is not None and remove_protocol:
            proxy = proxy.split('://', 1)[-1]
        return proxy

    @staticmethod
    def get_user_agent(spider, request):
        pp_noagent = getattr(spider, 'pp_noagent', False)
        if not pp_noagent:
            user_agent = request.headers.get('User-Agent')
            if user_agent is not None:
                if isinstance(user_agent, bytes):
                    user_agent = user_agent.decode('utf-8')
                return user_agent
        return None

    @staticmethod
    def generate_retry_request(request):
        # The original URL is in `puppeteer_origin_url` in `request.meta`
        orig_url = request.meta.get('puppeteer_origin_url')
        if orig_url is None:
            return None
        orig_keys = request.meta.get('puppeteer_original_meta_keys')
        if orig_keys is None:
            return None
        if 'retry_times' in request.meta:
            orig_keys += 'retry_time'  # Support for Scrapy retries

        orig_meta = request.meta.copy()
        new_request = request.replace(url=orig_url)
        # Some other conflictive values are there, so we need to remove them
        # Scrapy do not allow me to replace the request in the response object on the middleware
        # Or I just couldn't make it work yet...
        new_request.meta.clear()
        for meta_key in orig_keys:
            if meta_key in orig_meta:
                new_request.meta[meta_key] = orig_meta[meta_key]
        new_request.meta['recache'] = True
        new_request.dont_filter = True
        return new_request

    def process_request(self, request, spider):
        use_puppeteer = self.puppeteer_enabled(request, spider)
        puppeteer_processed = request.meta.get('puppeteer_processed', False)
        if use_puppeteer and (not puppeteer_processed):
            return self.generate_puppeteer_request(request, spider)
        elif request.meta.get('puppeteer_done', False):
            retry_request = self.generate_retry_request(request)
            if retry_request is not None:
                return retry_request

    @staticmethod
    def api_meta():
        '''Default meta values to use with a Puppeteer API Request'''
        return {
            'pp_enabled': False,
            'puppeteer_processed': True,
            'proxy': None,
            'keep_session': True,
            'proxy_service_disabled': True,  # Old API
            'ps_disabled': True,
            'handle_httpstatus_all': True,
            'cache_object_enabled': False,
        }

    @staticmethod
    def clean_meta(meta, *keep_meta):
        '''Remove the meta added by Puppeteer Middleware and some other redundant values'''
        def is_pp_meta(meta_key):
            other_meta = ['proxy', 'keep_session', 'proxy_service_disabled', 'handle_httpstatus_all',
                          'cache_object_enabled', 'no_proxy']
            if meta_key.startswith(('pp_', 'puppeteer')):
                return True
            return meta_key in other_meta

        new_meta = {}
        for meta_fld in meta:
            if is_pp_meta(meta_fld) and meta_fld not in keep_meta:
                continue
            new_meta[meta_fld] = meta[meta_fld]
        return new_meta

    def generate_puppeteer_request(self, request, spider):
        params = self.puppeteer_params(request, spider)

        request.meta['puppeteer_original_meta_keys'] = list(request.meta.keys())  # Used in retries

        request.meta['puppeteer_origin_proxy'] = request.meta.get('proxy')

        request.meta.update(self.api_meta())
        request.meta['puppeteer_origin_url'] = request.url

        puppeteer_url = self.puppeteer_url

        pp_view = self.puppeteer_config_value(request, spider, 'pp_view')
        if pp_view:
            puppeteer_url = urljoin(self.puppeteer_url, pp_view + '/')

        cookies = None

        api_url = urljoin(puppeteer_url, quote_plus(params['url']))
        for q_key, q_val in params.items():
            if q_key == 'url':
                continue
            if q_key == 'cookies':
                cookies = params[q_key]
                continue
            api_url = add_or_replace_parameter(api_url, q_key, q_val)

        spider.logger.info('PUPPETEER: New Request => %r', params['url'])

        if cookies is None:
            new_request = request.replace(
                url=api_url,
                method='GET',
            )
        else:
            formdata = {'cookies': cookies}
            new_request = FormRequest(
                url=api_url,
                formdata=formdata,
                meta=request.meta.copy(),
                callback=request.callback,
                dont_filter=True,
            )

        return new_request

    def process_exception(self, request, exception, spider):
        is_puppeteer_request = request.meta.get('puppeteer_processed', False)
        if is_puppeteer_request:
            if isinstance(exception, self.timeout_exceptions):
                return HtmlResponse(url=request.meta['puppeteer_origin_url'],
                                    status=504,
                                    request=request,
                                    body='')

    def process_response(self, request, response, spider):
        is_puppeteer_request = request.meta.get('puppeteer_processed', False)
        if is_puppeteer_request:
            resp = {'message': '',
                    'data': {'url': request.meta['puppeteer_origin_url'],
                             'body': '',
                             'cookies': [],
                             'evaluate': {}}}
            try:
                resp = json.loads(response.body.decode('utf-8'))
            except Exception as e:
                spider.logger.error('PUPPETEER: %r', e)
            if response.status != 200:
                spider.logger.warning('PUPPETEER: No 200 response => %d', response.status)

            try:
                resp_body = resp['data']['body'].encode('utf-8')
            except KeyError:
                resp_body = ''

            if 'errors' in resp:
                spider.logger.error('PUPETEER: errors found: %r', resp['errors'])

            request.meta['puppeteer_done'] = True
            request.meta['puppeteer_evaluate_res'] = resp['data'].get('evaluate', {})
            request.meta['puppeteer_cookies_res'] = resp['data'].get('cookies', [])

            response_type = request.meta.get('puppeteer_response_type', 'html')
            response_class = HtmlResponse
            if response_type == 'xml':
                response_class = XmlResponse
            url = request.meta['puppeteer_origin_url']
            if resp['data']['url'].startswith('http'):
                url = resp['data']['url']
            return response_class(url=url,
                                  status=response.status,
                                  request=request,
                                  body=resp_body,
                                  encoding='utf-8')
        return response
